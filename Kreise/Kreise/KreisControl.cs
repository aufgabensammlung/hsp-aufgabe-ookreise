﻿using System;

namespace Kreise
{
    class KreisControl
    {
        /// <summary>
        /// Konstruktor der Klasse
        /// </summary>
        KreisControl()
        {
        }

        /// <summary>
        /// In dieser Methode werden Kreise, also  erzeugt
        /// </summary>
        public void ErzeugeKreise()
        {
            new Kreis(); // Instanziierung eines Kreises
            // ToDo 4: Berechnen Sie die Fläche von mehreren Kreisen.

            // ToDo 5: Programmieren Sie Eingabe und Ausgaben.

            // ToDo 6a: Programmieren Sie einen neue Klasse Quadrat.
            // ToDo 6b: Ein Teamkollege programmiert eine neue Klasse Dreieck.
            // ToDo 6c: Bringen Sie alle Klassen zusammen per git-repo und berechnen Sie die Fläche von mehreren Formen. 
        }

        static void Main()
        {
            KreisControl kc = new KreisControl(); // Hier wird der Konstruktor von KreisControl aufgerufen und liefert ein Objekt vom Typ Kreis
            kc.ErzeugeKreise();                   // und hier wird eine Methode vom Objekt aufgerufen.

        }
    }

}
