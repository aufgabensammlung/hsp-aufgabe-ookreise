﻿using System;
using System.Drawing;

namespace Kreise
{
    public class Kreis
    {
        private double radius = 0;  // Radius des Kreises
        private Color farbe;        // Die Farbe des Kreises mit dem  Typ "Color" aus einer Bibliothek

        // ToDo 1a: Was fehlt noch für die vollständge Definition eins Kreises?

        /// <summary>
        /// Konstruktor der Klasse
        /// </summary>
        public Kreis()
        {
            // Dies ist der Konstruktor der Klasse. Immer, wenn ein Objekt dieser Klasse erzeugt wird, 
            // dann wird dieser als erstes ausgeführt.


            // ToDo 1b: Initialisierung der Zustandseigenschaften
            // Dies könnten Standardwerte sein x,y=0, r=1, Farbe=weiss, 
            // oder Sie können aber auch an den Konstruktor Parameter übergeben, 
            // welche Sie dann für die Initalisierung des Zustandes nutzen können.

            farbe = Color.White; // Beispiel für eine Farbzuweisung
        }

        /// <summary>
        /// Berechnung der Fläche des Kreises
        /// </summary>
        /// <returns>Fläche des Kreises</returns>
        public double getFlaeche()
        {
            return (Math.PI * Math.Pow(radius, 2));
        }

        public void setFarbe(Color farbe)
        {
            this.farbe = farbe;
        }

        // ToDo 2: Ergänzen Sie die Methode setRadius

        // ToDo 3: Ergänzen Sie die Methode getRadius


    }
}