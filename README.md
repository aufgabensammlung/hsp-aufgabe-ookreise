# HSP-Kreise
Grundlagen der Obejektorientierung werden in dieser Aufgabe analog zu der Vorlesung gezeigt. Erzeugen Sie mehrere Kreise (Objekte) von dem Typ Kreis.

Es existieren in dieser Aufgabe keine Unit-Tests, doch dafür nummerierte ToDos, welche Ihnen die Programmierschritte näher bringen sollen.

Finden Sie die Aufgaben (ToDo 1a, ToDo 1b, ToDo 2 .... ToDo 6) und lösen Sie diese.

## Getting Started
* Clonen Sie die Repository auf Ihren Rechner
* Öffnen Sie die Solution *.sln in Visual Studio
* Führen Sie die Implementierung in den Klassen durch

Testen Sie Ihr Programm, 
* indem Sie einen roten Breakpoint setzen und 
* die Debuging Funktion von Visual Studio nutzen.
* (es gibt keine Unit-Tests)
